## [Unreleased]

- New update

## [1.0.0] - 2021-07-01

-  Breaking change in the configuration of custom registry, see documentation
-  Add support to push in multiple registries
-  Add support to authentication in multiple registries

## [0.2.0] - 2021-06-22

- Add variable `DOCKERFILE_PATH` which permits specifying custom path to
  Dockerfile

## [0.1.0] - 2021-06-20

- Initial version
